## Introducción

Solución a la prueba técnica para el cargo de Desarrollador Full Stack en Forus.

### Estructura de la solución

Las soluciones a cada ejercicio están en las carpetas ejercicio_1, ejercicio_2 y ejercicio_3. Cada ejercicio se divide en 2 carpetas: frontend y backend.
Se exceptúa el ejercicio 1, que no presenta tareas de frontend.

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_1.jpg)

### Stack tecnológico

Para el frontend se utilizó Angular 9 y para el estilo se ocupó Angular Material.
Para el backend se utilizó Springboot

### Ejercicio 1

Como se mencionó, se ocupó Springboot para poder crear la API Rest. Se desarrollaron 5 endpoints:

1. GET /producto -> Entrega todos los productos existentes
2. GET /producto/{id} -> Entrega un producto dado su id
3. PUT /producto/{id} -> Actualiza un producto dado su id
4. POST /producto -> Crea un producto
5. DELETE /producto/{id} -> Elimina un producto por id

Se creó una interfaz que extiende la interfaz CrudRepository:

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_2.jpg)

Luego se creó una implementación de la interfaz anterior, que se encarga de almacenar los productos en memoria:

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_3.jpg)

Con esta solución, el problema se redujo al manejo de una lista enlazada para poder crear/actualizar/consultar y eliminar productos.


### Ejercicio 2

Para este ejercicio se creó otra implementación que extiende la interfaz IProductoService. En este caso, esta interfaz se conecta con la API de Forus 
mediante un cliente Rest (RestTemplate):

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_4.jpg)

En el controlador de la API, se instancia el servicio anterior:

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_5.jpg)

De esta manera, toda la comunicación será con la API de Forus.

Para el frontend, se utilizó creó un proyecto Angular (con Angular CLI). Esto permite manejar el desarrollo por componentes y módulos. Para este caso, se crearon 2 componentes claves:

1. app-producto-lista
2. app-producto-editor

El primero, se encarga de hacer una llamada al backend de este ejercicio para obtener el listado de productos y luego mostrarlos por la interfaz.
El segundo, se encarga de la lógica interna de un producto (el formulario para crear/editar).

### Ejercicio 3

La solución a este ejercicio es casi directa: el backend utilizado es el mismo del ejercicio 1 y el frontend es el mismo del ejercicio 2.
El frontend sabe a cuál backend apuntar gracias a que los backends corren en puertos distintos (8083 para el ejercicio 1 y 8082 para el ejercicio 2).
Esta configuración se guarda en el frontend en una clase llamada AppSettings:

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_6.jpg)

### Deploy

Para levantar el backend, es casi directo con VS Code. Se utilizó una extensión llamada Spring Boot Dashboard. Desde ahí se puede iniciar el server:

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_7.jpg)

Para levantar el frontend, en una consola nos paramos en la carpeta respectiva (ej: en el caso del ejercicio 2 sería ejercicio_2/frontend)
y ejecutamos el comando "ng serve". Esto hará que el frontend levante en localhost:4200

![alt text](https://gitlab.com/felipe.chacon/imagenes/-/raw/master/Screenshot_8.jpg)

### Contacto

Mi nombre es Felipe Ignacio Chacón Candia
Mi email es: felipe.chacon@gmail.com


