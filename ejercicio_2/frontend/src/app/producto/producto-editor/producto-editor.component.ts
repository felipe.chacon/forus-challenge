import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { Producto } from 'src/app/models/producto.model';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { forEach as _forEach } from 'lodash-es';

@Component({
	selector: 'app-producto-editor',
	templateUrl: './producto-editor.component.html',
	styleUrls: ['./producto-editor.component.scss'],
})
export class ProductoEditorComponent implements OnInit, OnChanges {

	@Input()
	producto: Producto;

	@Output()
	validoChange = new EventEmitter<boolean>();

	public formulario: FormGroup;

	constructor(private formBuilder: FormBuilder) {
		this.crearFormulario();
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes?.producto?.currentValue) {
			this.setearFormulario();
		}
	}

	ngOnInit() {

	}

	private crearFormulario(): void {
		this.formulario = this.formBuilder.group({
			nombre: ['', [Validators.required]],
			descripcion: ['', [Validators.required]],
			cantidad: ['', [Validators.required]],
			ubicacion: ['', [Validators.required]]
		});

		this.formulario.valueChanges.subscribe(form => {
			this.producto.productoNombre = form.nombre;
			this.producto.productoDescripcion = form.descripcion;
			this.producto.productoCantidad = form.cantidad;
			this.producto.productoUbicacion = form.ubicacion;
			this.validoChange.emit(this.formulario.valid);
		});
	}

	private setearFormulario(): void {
		this.formulario.patchValue({
			nombre: this.producto.productoNombre,
			descripcion: this.producto.productoDescripcion,
			cantidad: this.producto.productoCantidad,
			ubicacion: this.producto.productoUbicacion
		});

		this.validoChange.emit(this.formulario.valid);
		_forEach(this.formulario.controls, (c: AbstractControl) => {
			c.markAsDirty();
			c.markAsTouched();
		});
	}
}
