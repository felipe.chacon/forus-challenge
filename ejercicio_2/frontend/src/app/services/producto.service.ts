import { Injectable } from '@angular/core';
import { Producto } from '../models/producto.model';
import { HttpService } from './http.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { IProducto } from '../interfaces/producto.interface';
import { forEach as _forEach } from 'lodash-es';

@Injectable()
export class ProductoService {

	constructor(private httpService: HttpService) {

	}

	public obtenerProductosLista(): Observable<Array<Producto>> {
		const url = '';
		return this.httpService.getService(url)
			.pipe(
				map((productosItems: Array<IProducto>) => {
					const productos = new Array<Producto>();
					_forEach(productosItems, (productoItem: IProducto) => {
						const producto = new Producto();
						producto.productoId = productoItem.productoId;
						producto.productoNombre = productoItem.productoNombre;
						producto.productoDescripcion = productoItem.productoDescripcion;
						producto.productoCantidad = productoItem.productoCantidad;
						producto.productoUbicacion = productoItem.productoUbicacion;
						productos.push(producto);
					});

					return productos;
				}),
				catchError((error: HttpErrorResponse) => {
					console.error(error);
					return of(new Array<Producto>());
				})
			);
	}

	public crearProducto(producto: Producto): Observable<Producto> {
		const url = '';
		return this.httpService.postService(url, producto)
			.pipe(
				map((res: IProducto) => {
					producto.productoId = res.productoId;
					return producto;
				}),
				catchError((error: HttpErrorResponse) => {
					console.error(error);
					return of(null);
				})
			);
	}

	public actualizarProducto(producto: Producto): Observable<boolean> {
		const url = String(producto.productoId);
		return this.httpService.putService(url, producto)
			.pipe(
				map((res: any) => { return true; }),
				catchError((error: HttpErrorResponse) => {
					console.error(error);
					return of(false);
				})
			);
	}

	public eliminarProducto(productoId: number): Observable<boolean> {
		const url = String(productoId);
		return this.httpService.deleteService(url).pipe(
			map((res: any) => { return true; }),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return of(false);
			})
		);
	}
}
