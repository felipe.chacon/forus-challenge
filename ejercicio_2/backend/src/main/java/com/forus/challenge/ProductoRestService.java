package com.forus.challenge;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ProductoRestService implements IProductoService {

    private RestTemplate restTemplate;
    private String rutaBaseApi;

    public ProductoRestService(RestTemplate restTemplate, String rutaApi) {
        this.restTemplate = restTemplate;
        this.rutaBaseApi = rutaApi;
    }

    @Override
    public List<Producto> obtenerTodosLosProductos() {
        ResponseEntity<Producto[]> response = this.restTemplate.getForEntity(rutaBaseApi, Producto[].class);
        return Arrays.asList(response.getBody());
    }

    @Override
    public Producto obtenerProductoPorId(int productoId) {
        String endpoint = rutaBaseApi + String.valueOf(productoId);
        ResponseEntity<Producto> response = this.restTemplate.getForEntity(endpoint, Producto.class);
        return response.getBody();
    }

    @Override
    public boolean crearProducto(Producto producto) {
        String endpoint = rutaBaseApi;
        this.restTemplate.postForObject(endpoint, producto, String.class);
        return true;
    }

    @Override
    public void actualizarProducto(Producto producto) {
        String endpoint = rutaBaseApi;
        this.restTemplate.put(endpoint, producto);
    }

    @Override
    public void eliminarProducto(int productoId) {
        String endpoint = rutaBaseApi + "{id}";

        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(productoId));

        this.restTemplate.delete(endpoint, params);
    }
    
}