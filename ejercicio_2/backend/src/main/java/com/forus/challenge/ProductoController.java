package com.forus.challenge;

import java.util.List;
import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ProductoController {

    private final IProductoService productoService;
    private final static String rutaBaseApi = "https://sistemas.forus.cl/forus/challenge/dummy-api/producto/";

    public ProductoController() {
        productoService = new ProductoRestService(new RestTemplate(), rutaBaseApi);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/producto")
    public List<Producto> producto() {
        return this.productoService.obtenerTodosLosProductos();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/producto/{id}")
    public Producto productoPorId(@PathVariable final Integer id) {
        return this.productoService.obtenerProductoPorId(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/producto/{id}")
    public Producto actualizarProducto(@RequestBody final Producto producto, @PathVariable final Integer id) {
        this.productoService.actualizarProducto(producto);
        return producto;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/producto")
    public Producto crearProducto(@RequestBody final Producto producto) {
        
        int id = new Random().nextInt(5);
        id = id == 0 ? 1 : id;
        producto.setProductoId(id);

        if (this.productoService.crearProducto(producto)) {
            return producto;
        }

        return null;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/producto/{id}")
    public void eliminarProducto(@PathVariable final Integer id) {
        this.productoService.eliminarProducto(id);
    }
}