package com.forus.challenge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class ProductoService implements IProductoService {

    private IProductoRepository productoRepository;

    public ProductoService(IProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public List<Producto> obtenerTodosLosProductos() {
        List<Producto> productos = new ArrayList<Producto>();
        Iterator<Producto> iterator = this.productoRepository.findAll().iterator();

        while (iterator.hasNext()) {
            productos.add(iterator.next());
        }

        return productos;
    }

    @Override
    public Producto obtenerProductoPorId(int productoId) {
        Optional<Producto> producto = this.productoRepository.findById(productoId);
        return producto.isPresent() ? producto.get() : null;
    }

    @Override
    public synchronized boolean crearProducto(Producto producto) {
        return productoRepository.save(producto) != null;
    }

    @Override
    public void actualizarProducto(Producto producto) {
        productoRepository.save(producto);
    }

    @Override
    public void eliminarProducto(int productoId) {
       this.productoRepository.deleteById(productoId);
    }
    
}