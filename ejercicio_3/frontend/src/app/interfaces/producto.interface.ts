export interface IProducto {
	productoId: number;
	productoNombre: string;
	productoDescripcion: string;
	productoCantidad: number;
	productoUbicacion: number;
}
