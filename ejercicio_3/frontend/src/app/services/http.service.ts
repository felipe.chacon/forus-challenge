import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, timeoutWith } from 'rxjs/operators';
import { AppSettings } from '../app-settings';

export interface HttpClientOptions {
	headers?: HttpHeaders | {[header: string]: string | string[]; };
	observe?: 'body';
	params?: HttpParams | {[param: string]: string | string[]; };
	reportProgress?: boolean;
	withCredentials?: boolean;
}

@Injectable()
export class HttpService {
	headers: Headers;
	timeOutError: any;
	errorSubscriber: any;
	public httpClientOptions: HttpClientOptions;
	public httpParams: HttpParams;
	public httpHeaders: HttpHeaders;

	constructor(
		private httpClient: HttpClient
	) {

		this.httpHeaders = new HttpHeaders({
			'Content-Type': 'application/json'
		});

		this.httpClientOptions = {
			headers: this.httpHeaders
		};

		// Timeout Error Handler
		this.timeOutError = throwError(
			new Error('REQUEST_TIME_OUT_ERROR')
		);
	}

	getService(url: string): Observable<any> {
		return this.httpClient.get(`${AppSettings.API_URL}${url}`, this.httpClientOptions).pipe(
			catchError(this.handleError),
			timeoutWith(AppSettings.MAX_TIMEOUT_TIME, this.timeOutError)
		);
	}

	postService(url: string, param: any): Observable<any> {
		const body = JSON.stringify(param);
		return this.httpClient
			.post(`${AppSettings.API_URL}${url}`, body, this.httpClientOptions)
			.pipe(
				catchError(this.handleError),
				timeoutWith(AppSettings.MAX_TIMEOUT_TIME, this.timeOutError)
			);
	}

	putService(url: string, param: any): Observable<any> {
		const body = JSON.stringify(param);
		return this.httpClient
			.put(`${AppSettings.API_URL}${url}`, body, this.httpClientOptions)
			.pipe(
				catchError(this.handleError),
				timeoutWith(AppSettings.MAX_TIMEOUT_TIME, this.timeOutError)
			);
	}

	deleteService(url: string): Observable<any> {

		return this.httpClient
			.delete(`${AppSettings.API_URL}${url}`, this.httpClientOptions)
			.pipe(
				catchError(this.handleError),
				timeoutWith(AppSettings.MAX_TIMEOUT_TIME, this.timeOutError)
			);
	}

	private handleError(error: any) {
		if (error.status === 401 || error.status === 403) {
			window.location.href = `${AppSettings.APP_BASE_PATH}sin-autorizacion`;
		}

		if (error.status === 401 || error.status === 404) {
			window.location.href = `${AppSettings.APP_BASE_PATH}no-encontrado`;
		}

		const errMsg = error._body && JSON.parse(error._body).userMessage
			? JSON.parse(error._body).userMessage
			: 'Error del Servidor';

		return throwError({ status: error.status || 500, response: errMsg });
	}
}
