import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductoListaComponent } from './producto-lista/producto-lista.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductoEditorComponent } from './producto-editor/producto-editor.component';
import { ProductoService } from '../services/producto.service';
import { HttpService } from '../services/http.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

const routes: Routes = [
	{
		path: '',
		component: ProductoListaComponent,
	}
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		ReactiveFormsModule,
		MatExpansionModule,
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule
	],
	declarations: [
		ProductoListaComponent,
		ProductoEditorComponent
	],
	providers: [
		HttpService,
		ProductoService
	],
	exports: [],
})
export class ProductoModule {}
