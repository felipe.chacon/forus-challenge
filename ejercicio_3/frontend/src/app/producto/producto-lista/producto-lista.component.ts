import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto.model';
import { ProductoService } from 'src/app/services/producto.service';
import { ToastrService } from 'ngx-toastr';
import { forEach as _forEach } from 'lodash-es';

export interface ItemProductoAcordeon {
	producto: Producto;
	valido: boolean;
}

@Component({
	selector: 'app-producto-lista',
	templateUrl: './producto-lista.component.html',
	styleUrls: ['./producto-lista.component.scss'],
})
export class ProductoListaComponent implements OnInit {

	public productos = new Array<ItemProductoAcordeon>();
	public cargando = true;
	public productoAbierto = -1;
	public guardando = new Array<boolean>();
	public eliminando = new Array<boolean>();

	constructor(
		private productoService: ProductoService,
		private toastr: ToastrService
	) {

	}

	ngOnInit(): void {
		this.productoService.obtenerProductosLista().subscribe(
			(res: Array<Producto>) => {
				this.cargando = false;
				_forEach(res, (producto: Producto) => {
					this.productos.push({
						producto,
						valido: true
					});

					this.guardando.push(false);
					this.eliminando.push(false);
				});
			}, (error) => {
				this.toastr.error('Ocurrió un error al obtener los productos', 'Error!');
				this.cargando = false;
			});
	}

	public eliminarProducto(producto: Producto, index: number): void {

		this.eliminando[index] = true;

		setTimeout(() => {
			if (producto.productoId <= 0) {
				this.productos.splice(index, 1);
				this.guardando.splice(index, 1);
				this.eliminando.splice(index, 1);
			} else {
				this.productoService.eliminarProducto(producto.productoId).subscribe(
					(res: boolean) => {
						if (res) {
							this.productos.splice(index, 1);
							this.guardando.splice(index, 1);
							this.eliminando.splice(index, 1);
							this.productoAbierto = -1;
							this.toastr.success('El producto ha sido eliminado', 'Bien!');
						} else {
							this.toastr.error('El producto no pudo ser eliminado', 'Error!');
							this.eliminando[index] = false;
						}
					}, (error) => {
						this.toastr.error('El producto no pudo ser eliminado', 'Error!');
						this.eliminando[index] = false;
					}
				);
			}
		}, 1000);
	}

	public guardarProducto(productoItem: ItemProductoAcordeon, index: number): void {

		if (!productoItem.valido) {
			this.toastr.error('Hay errores en el formulario', 'Error!');
			return;
		}

		this.guardando[index] = true;

		setTimeout(() => {

			const producto = productoItem.producto;

			if (producto.productoId === 0) {
				this.productoService.crearProducto(producto).subscribe(
					(res: Producto) => {
						if (res) {
							producto.productoId = res.productoId;
							this.toastr.success('El producto se creó exitosamente', 'Bien!');
						} else {
							this.toastr.error('El producto no pudo ser creado', 'Error!');
						}

						this.guardando[index] = false;
					}, (error) => {
						this.toastr.error('El producto no pudo ser creado', 'Error!');
						this.guardando[index] = false;
					}
				);
			} else {
				this.productoService.actualizarProducto(producto).subscribe(
					(res: boolean) => {
						if (res) {
							this.toastr.success('El producto se actualizó exitosamente', 'Bien!');
						} else {
							this.toastr.error('El producto no pudo ser actualizado', 'Error!');
						}

						this.guardando[index] = false;
					}, (error) => {
						this.toastr.error('El producto no pudo ser actualizado', 'Error!');
						this.guardando[index] = false;
					}
				);
			}
		}, 1000);
	}

	public agregarProducto(): void {
		const producto = new Producto();
		producto.productoNombre = 'Producto sin nombre';
		producto.productoDescripcion = 'Producto sin descripción';
		this.productos.push({
			producto,
			valido: false
		});

		this.guardando.push(false);
		this.eliminando.push(false);

		this.setProductoAbierto(this.productos.length - 1);
	}

	public cambiarValido(valido: boolean, productoItem: ItemProductoAcordeon): void {
		productoItem.valido = valido;
	}

	public setProductoAbierto(i: number): void {
		this.productoAbierto = i;
	}
}
