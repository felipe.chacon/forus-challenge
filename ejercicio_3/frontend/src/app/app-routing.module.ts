import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/producto',
		pathMatch: 'full'
	}, {
		path: 'producto',
		loadChildren: () => import('./producto/producto.module').then(m => m.ProductoModule)
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
