export class Producto {

	productoId: number;
	productoNombre: string;
	productoDescripcion: string;
	productoCantidad: number;
	productoUbicacion: number;

	constructor() {
		this.productoId = 0;
	}
}
