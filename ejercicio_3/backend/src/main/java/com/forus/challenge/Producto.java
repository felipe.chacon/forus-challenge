package com.forus.challenge;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Producto {
    
    @Id
    private int productoId;
    private String productoNombre;
    private String productoDescripcion;
    private int productoCantidad;
    private int productoUbicacion;

    public Producto() {

    }

    public Producto(int productoId) {
        this.productoId = productoId;
    }

    public Producto(int productoId, String productoNombre) {
        this.productoId = productoId;
        this.productoNombre = productoNombre;
    }

    public int getProductoId() {
        return productoId;
    }

    public void setProductoId(int productoId) {
        this.productoId = productoId;
    }

    public String getProductoNombre() {
        return productoNombre;
    }

    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public int getProductoCantidad() {
        return productoCantidad;
    }

    public void setProductoCantidad(int productoCantidad) {
        this.productoCantidad = productoCantidad;
    }

    public int getProductoUbicacion() {
        return productoUbicacion;
    }

    public void setProductoUbicacion(int productoUbicacion) {
        this.productoUbicacion = productoUbicacion;
    }

    @Override
    public String toString() {
        return "Producto [productoCantidad=" + productoCantidad + ", productoDescripcion=" + productoDescripcion
                + ", productoId=" + productoId + ", productoNombre=" + productoNombre + ", productoUbicacion="
                + productoUbicacion + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + productoCantidad;
        result = prime * result + ((productoDescripcion == null) ? 0 : productoDescripcion.hashCode());
        result = prime * result + (int) (productoId ^ (productoId >>> 32));
        result = prime * result + ((productoNombre == null) ? 0 : productoNombre.hashCode());
        result = prime * result + productoUbicacion;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Producto other = (Producto) obj;
        
        if (productoId == other.productoId)
            return true;

        if (productoNombre == null) {
            if (other.productoNombre != null)
                return false;
        } else if (!productoNombre.equals(other.productoNombre)) {
            return false;
        }
       
        return true;
    }
}