package com.forus.challenge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ChallengeApplication {

	private static final Logger log = LoggerFactory.getLogger(ChallengeApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

	@Bean
	public IProductoService productoService() {
		return new ProductoService(ProductoEnMemoriaRepository.getInstancia());
	}

	@Bean
	public CommandLineRunner run(IProductoService productoService) throws Exception {
		return args -> {
			for (int i = 1; i <= 20; i++) {
				Producto producto = new Producto();
				producto.setProductoId(i);
				producto.setProductoNombre("Producto " + i);
				producto.setProductoDescripcion("Descripción " + i);
				producto.setProductoCantidad(100 + i);
				producto.setProductoUbicacion(200 + i);
				productoService.crearProducto(producto);
				log.info("Producto con id: " + i + " creado!");
			}
		};
	}
}
