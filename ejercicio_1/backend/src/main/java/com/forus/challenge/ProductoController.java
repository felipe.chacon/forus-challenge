package com.forus.challenge;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoController {

    private final IProductoService productoService;

    public ProductoController() {
        productoService = new ProductoService(ProductoEnMemoriaRepository.getInstancia());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/producto")
    public List<Producto> producto() {
        return this.productoService.obtenerTodosLosProductos();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/producto/{id}")
    public Producto productoPorId(@PathVariable final Integer id) {
        return this.productoService.obtenerProductoPorId(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/producto/{id}")
    public Producto actualizarProducto(@RequestBody final Producto producto, @PathVariable final Integer id) {
        this.productoService.actualizarProducto(producto);
        return this.productoService.obtenerProductoPorId(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/producto")
    public Producto crearProducto(@RequestBody final Producto producto) {
        if (this.productoService.crearProducto(producto)) {
            return this.productoService.obtenerProductoPorId(producto.getProductoId());
        }

        return null;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/producto/{id}")
    public void eliminarProducto(@PathVariable final Integer id) {
        this.productoService.eliminarProducto(id);
    }
}