package com.forus.challenge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class ProductoEnMemoriaRepository implements IProductoRepository {

    private final List<Producto> productos = new ArrayList<Producto>();
    private static final ProductoEnMemoriaRepository instancia = new ProductoEnMemoriaRepository();
    
    private ProductoEnMemoriaRepository() {

    }

    public static ProductoEnMemoriaRepository getInstancia() {
        return instancia;
    }

    @Override
    public <S extends Producto> S save(final S producto) {
        int indexOf = this.productos.indexOf(producto);
        if (indexOf >= 0) {
            this.productos.set(indexOf, producto);
        } else {
            this.productos.add(producto);
        }

        return producto;
    }

    @Override
    public <S extends Producto> Iterable<S> saveAll(final Iterable<S> entities) {
        Iterator<S> iterator = entities.iterator();

        while (iterator.hasNext()) {
            Producto producto = iterator.next();
            this.save(producto);
        }

        return entities;
    }

    @Override
    public Optional<Producto> findById(final Integer id) {
        Producto producto = this.productos.stream().filter(p -> p.getProductoId() == id).findFirst().orElse(null);
        return Optional.ofNullable(producto);
    }

    @Override
    public boolean existsById(final Integer id) {
        return this.findById(id).isPresent();
    }

    @Override
    public Iterable<Producto> findAll() {
        return this.productos;
    }

    @Override
    public Iterable<Producto> findAllById(final Iterable<Integer> ids) {
       List<Producto> subproductos = new ArrayList<Producto>();
       Iterator<Integer> iterator = ids.iterator();
       while (iterator.hasNext()) {
           Optional<Producto> producto = this.findById(iterator.next());
           if (producto.isPresent()) {
               subproductos.add(producto.get());
           }
       }
        return subproductos;
    }

    @Override
    public long count() {
        return this.productos.size();
    }

    @Override
    public void deleteById(final Integer id) {
        int index = this.productos.indexOf(new Producto(id));
        if (index >= 0) {
            this.productos.remove(index);
        }
    }

    @Override
    public void delete(final Producto entity) {
        this.deleteById(entity.getProductoId());
    }

    @Override
    public void deleteAll(final Iterable<? extends Producto> entities) {
        Iterator<? extends Producto> iterator = entities.iterator();
        while (iterator.hasNext()) {
            Producto producto = iterator.next();
            this.delete(producto);
        }
    }

    @Override
    public void deleteAll() {
        this.productos.clear();
    }
    
}