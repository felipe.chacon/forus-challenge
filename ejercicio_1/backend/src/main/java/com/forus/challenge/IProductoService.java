package com.forus.challenge;

import java.util.List;

public interface IProductoService {
    List<Producto> obtenerTodosLosProductos();

    Producto obtenerProductoPorId(int productoId);

    boolean crearProducto(Producto producto);

    void actualizarProducto(Producto producto);

    void eliminarProducto(int productoId);
}